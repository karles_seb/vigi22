package lesson7;

import java.sql.Time;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

public class Tank {
    private static final int MAX_X = 10;
    private static final int MAX_Y = 10;

    private int x;
    private int y;
    private Direction direction;
    private int[] shoot = {0, 0, 0, 0}; // Š,P,R,V

    public Tank() {
        x = 0;
        y = 0;
        direction = Direction.NORTH;
    }

    public void goNorth() {
        direction = Direction.NORTH;
        if (MAX_Y > y) {
            y++;
        }
        movePrint();
    }

    public void goSouth() {
        direction = Direction.SOUTH;
        if (-MAX_Y < y) {
            y--;
        }
        movePrint();
    }

    public void goEast() {
        direction = Direction.EAST;
        if (MAX_X > x) {
            x++;
        }
        movePrint();
    }

    public void goWest() {
        direction = Direction.WEST;
        if (-MAX_X < x) {
            x--;
        }
        movePrint();
    }

    public void shoot() {
        switch (direction) {
            case NORTH:
                shoot[0]++;
                break;
            case SOUTH:
                shoot[1]++;
                break;
            case EAST:
                shoot[2]++;
                break;
            case WEST:
                shoot[3]++;
                break;
        }
        shootPrint();
    }

    public void getInfo() {
        System.out.println("INFO: Tanko koordinates: (" + x + ";" + y + "), kryptis:" + direction.getDirection());
        System.out.println(
                "INFO: Tanko suviai: "
                        + shoot[0] + " i " + Direction.NORTH.getDirection() + ", "
                        + shoot[1] + " i " + Direction.SOUTH.getDirection() + ", "
                        + shoot[2] + " i " + Direction.EAST.getDirection() + ", "
                        + shoot[3] + " i " + Direction.WEST.getDirection() + ". "
                        + " Is viso suviu: " + (shoot[0] + shoot[1] + shoot[2] + shoot[3])

        );

    }

    private void shootPrint() {
        System.out.println("[" + timePrint() + "] Suvys i " + direction.getDirection());
    }

    private void movePrint() {
        System.out.println("[" + timePrint() + "] Tankas pajudejo i " + direction.getDirection() + " x: " + x + " y: " + y);
    }

    private String timePrint() {
        LocalTime time = LocalTime.now();

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm");
        String fTime = time.format(formatter);

        return fTime;
    }
}
