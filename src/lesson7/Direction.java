package lesson7;

public enum Direction {
    NORTH("Siaure"),
    SOUTH("Pietus"),
    EAST("Rytai"),
    WEST("Vakarai");

    private String direction;

    Direction(String direction) {
        this.direction = direction;
    }

    public String getDirection() {
        return direction;
    }
}
