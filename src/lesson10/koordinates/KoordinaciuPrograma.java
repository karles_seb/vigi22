import java.util.ArrayList;

public class KoordinaciuPrograma {

	public static void main(String[] args) {
		
		ArrayList<Koordinate> sarasas = new ArrayList<>();
		
		sarasas.add(new Koordinate(1, 5));
		sarasas.add(new Koordinate(5, 9));
		sarasas.add(new Koordinate(4, 0));
		sarasas.add(new Koordinate(0, 0));
		sarasas.add(new Koordinate(9, 1));
		
		int kelinta00 = -1;
		for(Koordinate k : sarasas) {
			System.out.println(k.getX() + "; " + k.getY());
			if (k.getX() == 0 && k.getY() == 0) {
				kelinta00 = sarasas.indexOf(k);
			}
		}
		System.out.println("(0;0) kordinate yra " + kelinta00);
		
		for(Koordinate k : sarasas) {
			if (k.getX() == 0 && k.getY() == 0) {
				k.setX(1);
				k.setY(1);
			}
		}
		
		for(Koordinate k : sarasas) {
			System.out.println(k.getX() + "; " + k.getY());
		}
		
		kelinta00 = -1;
		for(Koordinate k : sarasas) {
			if (k.getX() == 0 && k.getY() == 0) {
				kelinta00 = sarasas.indexOf(k);
			}
		}
		System.out.println("(0;0) kordinate yra " + kelinta00);
		
		
		
	}

}
