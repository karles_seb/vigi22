package lesson10;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

public class ArrayListTest {
    public static void main(String[] args) {
        ArrayList<String> miestai = new ArrayList<>();
        miestai.add("Vilnius");
        miestai.add("Kaunas");
        miestai.add("Klaipeda");
        miestai.add("Siauliai");

        ArrayList<String> miestai2 = new ArrayList<>();
        miestai2.add("Panevezys");
        miestai2.add("Anyksciai");

        for (int i = 0; i < miestai.size(); i++) {
            System.out.println(miestai.get(i));
        }
        System.out.println("----------------");
        for (String miestas : miestai) {
            System.out.println(miestas);
        }

        int counter = 0;
        while (miestai.size() > counter) {
            System.out.println(miestai.get(counter));
            counter++;
        }
        System.out.println("----------------");
        Iterator<String> iterator = miestai.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }

        System.out.println(miestai);
        Collections.sort(miestai);
        System.out.println(miestai);

        Collections.sort(miestai, Collections.reverseOrder());
        System.out.println(miestai);

        System.out.println(miestai);
        miestai.addAll(miestai2);
        System.out.println(miestai);

        System.out.println(miestai);
        Collections.swap(miestai, 0, 2);
        System.out.println(miestai);

        ArrayList<Integer> skaiciai = new ArrayList();
        skaiciai.add(9);
        skaiciai.add(15);
        skaiciai.add(52);
        skaiciai.add(1);

        System.out.println(skaiciai);

        Integer skaicius = skaiciai.get(1);
        int skaicius2 = skaiciai.get(1);

        System.out.println(skaicius);
        System.out.println(skaicius2);


    }
}
