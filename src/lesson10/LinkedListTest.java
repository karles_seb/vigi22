package lesson10;

import java.util.LinkedList;

public class LinkedListTest {
    public static void main(String[] args) {
        LinkedList<String> menesiai = new LinkedList<>();
        menesiai.add("vasaris");
        menesiai.add("kovas");
        menesiai.add("balandis");
        menesiai.add("geguze");
        System.out.println(menesiai);

        menesiai.addFirst("sausis");
        menesiai.addLast("birzelis");
        System.out.println(menesiai);

        menesiai.removeFirst();
        menesiai.removeLast();
        System.out.println(menesiai);

        System.out.println(menesiai.poll());
        System.out.println(menesiai);

        menesiai.pollFirst();
        System.out.println(menesiai);

        menesiai.pollLast();
        System.out.println(menesiai);
//
        menesiai.push("sausis");
        System.out.println(menesiai);

        String menesis = menesiai.pop();
        System.out.println(menesis);
        System.out.println(menesiai);

    }
}
