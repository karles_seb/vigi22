package lesson10;

import java.util.ArrayList;

public class Uzduotis1 {
    public static void main(String[] args) {
        ArrayList<Float> skaiciai = new ArrayList<>();
        skaiciai.add(15.26f);
        skaiciai.add(36.6f);
        skaiciai.add(88.26f);
        skaiciai.add(4.26f);
        skaiciai.add(0.26f);

        System.out.println(skaiciai);

        skaiciai.remove(1);
        skaiciai.remove(0.26f);
        System.out.println(skaiciai);
        skaiciai.clear();
        System.out.println(skaiciai);
    }
}
