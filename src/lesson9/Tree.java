package lesson9;

public class Tree {
    private String value;

    public Tree(String value){
        this.value = value;
    }

    @Override
    public boolean equals(Object obj) {
        return value.equals(((Tree) obj).value);
    }
}
