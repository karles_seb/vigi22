package lesson9;

public class Grass {
    public static void main(String[] args) {
        Tree tree1 = new Tree("as");
        Tree tree2 = new Tree("asd");

        System.out.println(tree1 == tree2);
        System.out.println(tree1.equals(tree2));

        tree1 = tree2;
        System.out.println(tree1 == tree2);
    }
}
