package lesson9;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        ArrayList<String> list = new ArrayList<>();

        while (list.size() < 6) {
            String value = sc.next();
            if (list.contains(value)) {
                System.out.println("Klaida");
            } else {
                list.add(value);
            }

            System.out.println(list);

            if (list.size() >= 5) {
                list.clear();
                System.out.println("Sarasas isvalytas");
            }

        }
        sc.close();
    }
}