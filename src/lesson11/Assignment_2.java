package lesson11;

import java.util.ArrayList;
import java.util.HashSet;

public class Assignment_2 {
    public static void main(String[] args) {
        ArrayList<String> vardai = new ArrayList<>();
        vardai.add("Tomas");
        vardai.add("Tomas");
        vardai.add("Petras");
        vardai.add("Petras");
        vardai.add("Petras");
        vardai.add("Paulius");
        vardai.add("Paulius");
        vardai.add("Jonas");
        System.out.println(vardai);

        HashSet<String> vardaiSet = new HashSet<>();
        for (String vardas: vardai){
            vardaiSet.add(vardas);
        }

        System.out.println(vardaiSet);
    }
}
