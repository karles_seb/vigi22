package lesson11;

import java.util.HashSet;

public class AibeTest {
    public static void main(String[] args) {
        HashSet<String> aibe = new HashSet<>();

        aibe.add("Obuolys");
        aibe.add("Pomidoras");
        aibe.add("Apelsinas");
        aibe.add("Kivis");

        System.out.println(aibe);

        aibe.remove("Obuolys");

        System.out.println(aibe);

//        Iterator<String> iterator = aibe.iterator();
//        while (iterator.hasNext()){
//            System.out.println(iterator.next());
//        }
//
//        System.out.println("-----");
//
//        for (String vaisius: aibe){
//            System.out.println(vaisius);
//        }

//        aibe.clear();
//
//        System.out.println(aibe);
//        System.out.println(aibe.isEmpty());
    }
}
