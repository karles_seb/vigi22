package lesson11;

import java.util.HashMap;
import java.util.Map;

public class AssignmentMap_1 {
    public static void main(String[] args) {
        HashMap<Integer, String> map = new HashMap<>();
        map.put(1, "vienas");
        map.put(11, "vienuolika");
        map.put(12, "dvylika");
        map.put(33, "trysdesimtrys");
        map.put(100, "simtas");

        System.out.println(map);
        System.out.println(map.containsKey(100));
        System.out.println(map.containsValue("simtas"));
        System.out.println(map.get(12));

        spausdintiMap(map);
        map.remove(11);
        spausdintiMap(map);
        map.clear();
        spausdintiMap(map);

    }

    private static void spausdintiMap(HashMap<Integer, String> mapas) {
        for (Map.Entry entry : mapas.entrySet()) {
            Integer key = (Integer) entry.getKey();
            System.out.println("Raktas: " + key + ", Reiksme: " + mapas.get(key));
        }
        System.out.println("----------------------------");
    }
}
