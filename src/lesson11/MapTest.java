package lesson11;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class MapTest {
    public static void main(String[] args) {
        HashMap<Integer, String> mapas = new HashMap<>();
        mapas.put(1, "Sausis");
        mapas.put(2, "Vasaris");
        mapas.put(3, "Kovas");
        mapas.put(88, "Balandis");
        mapas.put(33, "Balandis");

        System.out.println(mapas);
        System.out.println(mapas.get(88));
        System.out.println(mapas.keySet());
        System.out.println(mapas.entrySet());
        System.out.println();

//        Set entrySet = mapas.entrySet();
//        Iterator iterator = entrySet.iterator();
//        while (iterator.hasNext()) {
//            Map.Entry entry = (Map.Entry) iterator.next();
//            Integer raktas = (Integer) entry.getKey();
//            System.out.println("Raktas:" + raktas + ", Reiksme: " + mapas.get(raktas));
//        }
        System.out.println("------");
        for(Map.Entry entry : mapas.entrySet()){
            Integer raktas = (Integer) entry.getKey();
            System.out.println("Raktas:" + raktas + ", Reiksme: " + mapas.get(raktas));
        }

        System.out.println(mapas.size());
        System.out.println(mapas.remove(33));
        System.out.println(mapas);

        System.out.println(mapas.containsKey(88));
        System.out.println(mapas.containsValue("Sausis"));
        mapas.clear();
        System.out.println(mapas.isEmpty());
        System.out.println(mapas);

    }
}
