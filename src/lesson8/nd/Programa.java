package lesson8.nd;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Scanner;

public class Programa {
    private static Scanner scanner;
    private static Biudzetas biudzetas;

    public static void main(String[] args) {
        scanner = new Scanner(System.in);
        biudzetas = new Biudzetas(50);
        boolean runProgram = true;

        while (runProgram) {
            System.out.println("[1] - Prideti islaida\n[2] - Prideti pajama\n[3] - Isvesti islaida\n[4] - Isvesti pajama\n[0] - Isjungti programa");
            int pasirinkimas = scanner.nextInt();
            switch (pasirinkimas) {
                case 1:
                    islaidosPridejimas();
                    break;
                case 2:
                    pajamosPridejimas();
                    break;
                case 3:
                    isvestiIslaida();
                    break;
                case 4:
                    isvestiPajama();
                    break;
                case 0:
                    runProgram = false;
                    break;
            }
        }
        scanner.close();
    }

    private static void islaidosPridejimas() {
        System.out.printf("[1] -%s\n[2] -%s\n[3] -%s\n", IslaiduKategorija.MOKESCIAI, IslaiduKategorija.DOVANA, IslaiduKategorija.NUOMA);
        int pasirinkimas = scanner.nextInt();
        IslaiduKategorija kategorija = null;
        switch (pasirinkimas) {
            case 1:
                kategorija = IslaiduKategorija.MOKESCIAI;
                break;
            case 2:
                kategorija = IslaiduKategorija.DOVANA;
                break;
            case 3:
                kategorija = IslaiduKategorija.NUOMA;
                break;
        }

        System.out.printf("[1] -%s\n[2] -%s\n", AtsiskaitymoBudas.GRYNAIS, AtsiskaitymoBudas.BANKU);
        pasirinkimas = scanner.nextInt();
        AtsiskaitymoBudas atsiskaitymoBudas = null;
        switch (pasirinkimas) {
            case 1:
                atsiskaitymoBudas = AtsiskaitymoBudas.GRYNAIS;
                break;
            case 2:
                atsiskaitymoBudas = AtsiskaitymoBudas.BANKU;
                break;
        }
        System.out.println("Iveskite suma:");
        float suma = scanner.nextFloat();
        IslaiduIrasas islaiduIrasas = new IslaiduIrasas(suma, LocalDateTime.now(), kategorija, atsiskaitymoBudas);
        biudzetas.pridetiIslaiduIrasa(islaiduIrasas);
    }

    private static void pajamosPridejimas() {
        System.out.printf("[1] -%s\n[2] -%s\n[3] -%s\n", PajamuKategorija.DARBAS, PajamuKategorija.KYSIS, PajamuKategorija.VOKELIS);
        int pasirinkimas = scanner.nextInt();
        PajamuKategorija kategorija = null;
        switch (pasirinkimas) {
            case 1:
                kategorija = PajamuKategorija.DARBAS;
                break;
            case 2:
                kategorija = PajamuKategorija.KYSIS;
                break;
            case 3:
                kategorija = PajamuKategorija.VOKELIS;
                break;
        }

        System.out.println("Iveskite suma:");
        float suma = scanner.nextFloat();
        PajamuIrasas pajamuIrasas = new PajamuIrasas(suma, LocalTime.now(), kategorija);
        biudzetas.pridetiPajamuIrasa(pajamuIrasas);
    }

    private static void isvestiIslaida() {
        System.out.printf("Pasirinkite islaidos indeksa: 0 - %d\n", biudzetas.gautiIslaiduMasyvoKieki() - 1);
        int pasirinkimas = scanner.nextInt();
        IslaiduIrasas islaiduIrasas = biudzetas.gautiIslaiduIrasa(pasirinkimas);
        islaiduIrasas.atspausdintiIslaiduIrasa();
    }

    private static void isvestiPajama() {
        System.out.printf("Pasirinkite pajamos indeksa: 0 - %d\n", biudzetas.gautiPajamuMasyvoKieki() - 1);
        int pasirinkimas = scanner.nextInt();
        PajamuIrasas pajamuIrasas = biudzetas.gautiPajamuIrasa(pasirinkimas);
        pajamuIrasas.atspausdintiPajamuIrasa();
    }
}
