package lesson8.nd;

public class Biudzetas {
    private final static int MASYVO_DYDIS = 100;
    private PajamuIrasas[] pajamuIrasoMasyvas;
    private IslaiduIrasas[] islaiduIrasoMasyvas;

    public Biudzetas() {
        pajamuIrasoMasyvas = new PajamuIrasas[MASYVO_DYDIS];
        islaiduIrasoMasyvas = new IslaiduIrasas[MASYVO_DYDIS];
    }

    public Biudzetas(int masyvoDydis) {
        pajamuIrasoMasyvas = new PajamuIrasas[masyvoDydis];
        islaiduIrasoMasyvas = new IslaiduIrasas[masyvoDydis];
    }

    public void pridetiPajamuIrasa(PajamuIrasas pajamuIrasas) {
        for (int i = 0; i < pajamuIrasoMasyvas.length; i++) {
            if (pajamuIrasoMasyvas[i] == null) {
                pajamuIrasoMasyvas[i] = pajamuIrasas;
                break;
            }
        }
    }

    public void pridetiIslaiduIrasa(IslaiduIrasas islaiduIrasas) {
        for (int i = 0; i < islaiduIrasoMasyvas.length; i++) {
            if (islaiduIrasoMasyvas[i] == null) {
                islaiduIrasoMasyvas[i] = islaiduIrasas;
                break;
            }
        }
    }

    public PajamuIrasas gautiPajamuIrasa(int index) {
        return pajamuIrasoMasyvas[index];
    }

    public IslaiduIrasas gautiIslaiduIrasa(int index) {
        return islaiduIrasoMasyvas[index];
    }

    public int gautiPajamuMasyvoKieki() {
        int kiekis = 0;
        for (int i = 0; i < pajamuIrasoMasyvas.length; i++) {
            if (pajamuIrasoMasyvas[i] != null) {
                kiekis++;
            } else {
                break;
            }
        }
        return kiekis;
    }

    public int gautiIslaiduMasyvoKieki() {
        int kiekis = 0;
        for (int i = 0; i < islaiduIrasoMasyvas.length; i++) {
            if (islaiduIrasoMasyvas[i] != null) {
                kiekis++;
            } else {
                break;
            }
        }
        return kiekis;
    }

}
