package lesson8.nd;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

public class PajamuIrasas {
    private final static DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("HH:mm");
    private float suma;
    private PajamuKategorija kategorija;
    private LocalTime data;
    private boolean gautaIBanka;
    private String papildomaInfo;

    public PajamuIrasas(float suma, LocalTime data, PajamuKategorija kategorija) {
        this.suma = suma;
        this.data = data;
        this.kategorija = kategorija;
        if (PajamuKategorija.DARBAS.equals(kategorija)) {
            this.gautaIBanka = true;
        } else {
            this.gautaIBanka = false;
        }
    }

    public void atspausdintiPajamuIrasa() {
        System.out.printf("[%s] Suma: %.2f, kategorija: %s, ar gauta i banka: %b\n", data.format(FORMATTER), suma, kategorija, gautaIBanka);
    }

    public float getSuma() {
        return suma;
    }

    public void setSuma(float suma) {
        this.suma = suma;
    }

    public PajamuKategorija getKategorija() {
        return kategorija;
    }

    public void setKategorija(PajamuKategorija kategorija) {
        this.kategorija = kategorija;
    }

    public LocalTime getData() {
        return data;
    }

    public void setData(LocalTime data) {
        this.data = data;
    }

    public boolean isGautaIBanka() {
        return gautaIBanka;
    }

    public void setGautaIBanka(boolean gautaIBanka) {
        this.gautaIBanka = gautaIBanka;
    }

    public String getPapildomaInfo() {
        return papildomaInfo;
    }

    public void setPapildomaInfo(String papildomaInfo) {
        this.papildomaInfo = papildomaInfo;
    }
}
