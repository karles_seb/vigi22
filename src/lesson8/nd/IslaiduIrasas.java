package lesson8.nd;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class IslaiduIrasas {
    private final static DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("YYYY-MM-DD HH:mm");
    private float suma;
    private LocalDateTime data;
    private String papildomaInfo;
    private IslaiduKategorija kategorija;
    private AtsiskaitymoBudas atsiskaitymoBudas;
    private boolean isBanko;

    public IslaiduIrasas(float suma, LocalDateTime data, IslaiduKategorija kategorija, AtsiskaitymoBudas atsiskaitymoBudas) {
        this.suma = suma;
        this.data = data;
        this.kategorija = kategorija;
        this.atsiskaitymoBudas = atsiskaitymoBudas;
        if (AtsiskaitymoBudas.BANKU.equals(atsiskaitymoBudas)) {
            this.isBanko = true;
        } else {
            this.isBanko = false;
        }
    }

    public void atspausdintiIslaiduIrasa() {
        System.out.printf("[%s] Suma: %.2f, kategorija %s, atsiskaitymo budas: %s, ar gauta is banko: %b\n",
                data.format(FORMATTER),
                suma,
                kategorija,
                atsiskaitymoBudas,
                isBanko);
    }

    public float getSuma() {
        return suma;
    }

    public void setSuma(float suma) {
        this.suma = suma;
    }

    public LocalDateTime getData() {
        return data;
    }

    public void setData(LocalDateTime data) {
        this.data = data;
    }

    public String getPapildomaInfo() {
        return papildomaInfo;
    }

    public void setPapildomaInfo(String papildomaInfo) {
        this.papildomaInfo = papildomaInfo;
    }

    public IslaiduKategorija getKategorija() {
        return kategorija;
    }

    public void setKategorija(IslaiduKategorija kategorija) {
        this.kategorija = kategorija;
    }

    public AtsiskaitymoBudas getAtsiskaitymoBudas() {
        return atsiskaitymoBudas;
    }

    public void setAtsiskaitymoBudas(AtsiskaitymoBudas atsiskaitymoBudas) {
        this.atsiskaitymoBudas = atsiskaitymoBudas;
    }

    public boolean isBanko() {
        return isBanko;
    }

    public void setBanko(boolean banko) {
        isBanko = banko;
    }
}
